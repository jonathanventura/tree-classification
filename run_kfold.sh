#!/bin/bash
for i in 0 1 2 3 4 5 6 7 8 9 ;
do
  python trainpixel.py --out kfold$i --norm pca --balance ;
  python test.py --out kfold$i --norm pca;
  python analyze.py --out kfold$i ;
  python rf_train.py --out kfold$i --balance;
  python rf_test.py --out kfold$i ;
  python analyze.py --out kfold$i  --rf ;
done
