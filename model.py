from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, Activation, Flatten, Dense, Dropout, Lambda, MaxPooling2D, Cropping2D
from tensorflow.keras import regularizers
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K

class TreeClassifier:
    def __init__(self):
        reg = regularizers.l2(0.001)
        init = None #'he_normal'
        num_filt = 32
        max_num_filt = 128
        filt_sizes = [3,3,3,3,3,3,3]
        #filt_sizes = [5,5,5,3]
        #filt_sizes = [7,7,3]

        self.convs = []
        for filt_size in filt_sizes:
          self.convs.append(Conv2D(num_filt,filt_size,padding='valid',activation='relu',kernel_regularizer=reg,kernel_initializer=init))
          if num_filt < max_num_filt:
            num_filt *= 2
        #self.convs.append(Conv2D(64,1,padding='valid',activation='relu',kernel_regularizer=reg,kernel_initializer=init))
        #self.convs.append(Dropout(0.5))
        #self.convs.append(Conv2D(32,1,padding='valid',activation='relu',kernel_regularizer=reg,kernel_initializer=init))
        #self.convs.append(Dropout(0.5))
        self.convs.append(Conv2D(8,1,activation='softmax',kernel_regularizer=reg,kernel_initializer=init))

    def get_convolutional_model(self,input_shape):
        inputs = Input(input_shape)
        x = inputs

        for conv in self.convs:
          x = conv(x)
        outputs = x

        return Model(inputs=inputs,outputs=outputs)

    def get_patch_model(self,input_shape):
        conv_model = self.get_convolutional_model(input_shape)

        inputs = Input(input_shape)
        x = inputs

        for conv in self.convs:
          x = conv(x)
        outputs = Flatten()(x)

        return Model(inputs=inputs,outputs=outputs)

