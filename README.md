### CNN for Tree Classification from Hyperspectral data ###

Files needed:

* hyperspectral image: data/NEON\_D17\_TEAK\_DP1\_20170627\_181333\_reflectance.tif
* canopy height model: data/D17\_CHM\_all.tif
* labels shapefile: data/Labels\_Trimmed\_Selective.shp

---

To prepare training data:

    python preprocess.py --out output

To run training process:

    python train.py --out output --norm pca

To produce the prediction tiff:

    python test.py --out output --norm pca
