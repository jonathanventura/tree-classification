#!/bin/bash
for i in 0 1 2 3 4 5 6 7 8 9 ;
do
  python trainpixel.py --out kfold_rgb$i --norm meanstd --balance --epochs 40;
  python test.py --out kfold_rgb$i --norm meanstd --rgb;
  python analyze.py --out kfold_rgb$i ;
done
