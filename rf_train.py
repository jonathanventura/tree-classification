import numpy as np

import h5py as h5
from tqdm import tqdm, trange

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import train_test_split

import os
import sys

from paths import *

from joblib import dump, load

import argparse

np.random.seed(0)

parser = argparse.ArgumentParser()
parser.add_argument('--out',help='directory for output files')
parser.add_argument('--balance',action='store_true',help='use class weights')

args = parser.parse_args()

with h5.File(args.out + '/' + train_data_uri,'r') as f:
  x_all = f['data'][:,7,7].astype('float32')
  y_all = f['label'][:]

"""
with h5.File(args.out + '/' + train_data_uri,'r') as f:
  x_train = f['data'][:,7,7].astype('float32')
  y_train = f['label'][:]

with h5.File(args.out + '/' + val_data_uri,'r') as f:
  x_val = f['data'][:,7,7].astype('float32')
  y_val = f['label'][:]

x_all = np.concatenate([x_train,x_val],axis=0)
y_all = np.concatenate([y_train,y_val],axis=0)
"""
train_inds, val_inds = train_test_split(range(len(x_all)),test_size=0.1,random_state=0)
x_train = np.stack([x_all[i] for i in train_inds],axis=0)
y_train = np.stack([y_all[i] for i in train_inds],axis=0)
x_val = np.stack([x_all[i] for i in val_inds],axis=0)
y_val = np.stack([y_all[i] for i in val_inds],axis=0)

pca = PCA(32,whiten=True)
x_train = pca.fit_transform(x_train)
x_val = pca.transform(x_val)
print(x_train.shape)

if args.balance:
  model = RandomForestClassifier(n_estimators=100,class_weight='balanced')
else:
  model = RandomForestClassifier(n_estimators=100)

model.fit(x_train,y_train)

print(model.score(x_val,y_val))

dump(pca,args.out + '/rf_pca.joblib')
dump(model,args.out + '/rf_model.joblib')
