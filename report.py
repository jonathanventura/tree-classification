import numpy as np

import sklearn.metrics
from sklearn.metrics import confusion_matrix, classification_report

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--preds',help='predictions text file')
parser.add_argument('--labels',help='labels text file')
args = parser.parse_args()

test_labels = np.loadtxt(args.labels)
test_preds = np.loadtxt(args.preds)

report = classification_report(test_labels, test_preds)
print(report)
mat = confusion_matrix(test_labels,test_preds)
print(mat)
