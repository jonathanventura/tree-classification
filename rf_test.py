import numpy as np

import rasterio
from rasterio.windows import Window
from rasterio.enums import Resampling
from rasterio.vrt import WarpedVRT

import tqdm

from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA

from math import floor, ceil

from paths import *

from joblib import dump, load

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--out',help='directory for output files')
parser.add_argument('--rgb',action='store_true',help='use RGB data instead of hyperspectral')
args = parser.parse_args()

# "no data value" for labels
label_ndv = 255

# radius of square patch (side of patch = 2*radius+1)
patch_radius = 0

# height threshold for CHM -- pixels at or below this height will be discarded
height_threshold = 5

# tile size for processing
tile_size = 128

# tile size with padding
padded_tile_size = tile_size + 2*patch_radius

# open the hyperspectral or RGB image
if args.rgb:
  image = rasterio.open(rgb_image_uri)
else:
    image = rasterio.open(image_uri)
image_meta = image.meta.copy()
image_ndv = image.meta['nodata']
image_width = image.meta['width']
image_height = image.meta['height']
image_channels = image.meta['count']

# load model
input_shape = (padded_tile_size,padded_tile_size,image_channels)
model = load(args.out + '/rf_model.joblib')
pca = load(args.out + '/rf_pca.joblib')

# calculate number of tiles
num_tiles_y = ceil(image_height / float(tile_size))
num_tiles_x = ceil(image_width / float(tile_size))

print('Metadata for image')
for key in image_meta.keys():
  print('%s:'%key)
  print(image_meta[key])
  print()

# create predicted label raster
predict_meta = image_meta.copy()
predict_meta['dtype'] = 'uint8'
predict_meta['nodata'] = label_ndv
predict_meta['count'] = 1
predict = rasterio.open(args.out + '/rf_' + predict_uri, 'w', compress='lzw', **predict_meta)

# open the CHM
chm = rasterio.open(chm_uri)
chm_vrt = WarpedVRT(chm, crs=image.meta['crs'], transform=image.meta['transform'], width=image.meta['width'], height=image.meta['height'],
                   resampling=Resampling.bilinear)

# go through all tiles of input image
# run convolutional model on tile
# write labels to output label raster
with tqdm.tqdm(total=num_tiles_y*num_tiles_x) as pbar:
    for y in range(patch_radius,image_height-patch_radius,tile_size):
        for x in range(patch_radius,image_width-patch_radius,tile_size):
            pbar.update(1)

            window = Window(x-patch_radius,y-patch_radius,padded_tile_size,padded_tile_size)
            output_window = Window(x,y,tile_size,tile_size)

            # get tile from chm
            chm_tile = chm_vrt.read(1,window=window)
            if chm_tile.shape[0] != padded_tile_size or chm_tile.shape[1] != padded_tile_size: continue
            chm_tile = np.expand_dims(chm_tile,axis=0)
            chm_bad = chm_tile <= height_threshold

            # get tile from image
            image_tile = image.read(window=window)
            if image_tile.shape[1] != padded_tile_size or image_tile.shape[2] != padded_tile_size: continue

            # re-order image tile to have height,width,channels
            image_tile = np.transpose(image_tile,axes=[1,2,0])

            # add batch axis
            image_tile = np.expand_dims(image_tile,axis=0)
            image_bad = np.any(image_tile < 0,axis=-1)

            # remove mean and std. dev.
            image_tile = image_tile.astype('float32')
            image_tile = np.reshape(image_tile,(-1,image_tile.shape[-1]))
            image_tile = pca.transform(image_tile)

            # run tile through network
            predict_tile = model.predict(image_tile).astype('uint8')
            predict_tile = np.reshape(predict_tile,(1,padded_tile_size,padded_tile_size))

            # set bad pixels to NDV
            predict_tile[chm_bad] = label_ndv
            predict_tile[image_bad] = label_ndv

            # write to file
            predict.write(predict_tile,window=output_window)

image.close()
chm.close()
predict.close()
