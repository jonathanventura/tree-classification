#!/bin/bash
rm -r kfold_combined ;
mkdir -p kfold_combined ;
for i in 0 1 2 3 4 5 6 7 8 9 ;
do
  cat kfold$i/preds.txt >> kfold_combined/preds.txt;
  cat kfold$i/labels.txt >> kfold_combined/labels.txt;
  cat kfold$i/rf_preds.txt >> kfold_combined/rf_preds.txt;
  cat kfold$i/rf_labels.txt >> kfold_combined/rf_labels.txt;
done
python report.py --preds kfold_combined/preds.txt --labels kfold_combined/labels.txt > kfold_combined/report.txt;
python report.py --preds kfold_combined/rf_preds.txt --labels kfold_combined/rf_labels.txt > kfold_combined/rf_report.txt;
