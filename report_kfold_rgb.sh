#!/bin/bash
rm -r kfold_rgb_combined ;
mkdir -p kfold_rgb_combined ;
for i in 0 1 2 3 4 5 6 7 8 9 ;
do
  cat kfold_rgb$i/preds.txt >> kfold_rgb_combined/preds.txt;
  cat kfold_rgb$i/labels.txt >> kfold_rgb_combined/labels.txt;
  #cat kfold_rgb$i/rf_preds.txt >> kfold_rgb_combined/rf_preds.txt;
  #cat kfold_rgb$i/rf_labels.txt >> kfold_rgb_combined/rf_labels.txt;
done
python report.py --preds kfold_rgb_combined/preds.txt --labels kfold_rgb_combined/labels.txt > kfold_rgb_combined/report.txt;
#python report.py --preds kfold_rgb_combined/rf_preds.txt --labels kfold_rgb_combined/rf_labels.txt > kfold_rgb_combined/rf_report.txt;
